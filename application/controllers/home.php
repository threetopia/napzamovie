<?php
class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('content');
	}
	
	public function index()
	{
		$cast_list = $this->content->cast_process();
		$about = $this->content->about();
		$crew = $this->content->crew();
		$cast = $this->content->cast();
		$this->load->view('index_page',array('cast_list'=>$cast_list,'about'=>$about,'crew'=>$crew),false);
	}
}
?>