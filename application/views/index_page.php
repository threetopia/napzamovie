<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Film yang digarap berdasarkan kisah nyata tentang kelamnya kehidupan akibat penyalahgunaan Napza." />
<meta name="keywords" content="napza, narkotika, narkoba, obat terlarang" />
<title>NAPZA-Movie</title>
<link href="style/css/style.css" type="text/css" rel="stylesheet" media="all" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function(){
  $("#home_btn").addClass("selected");
  $("#about_btn,#cast_btn,#crew_btn").addClass("btn");
  $("#home_btn").click(function(){
	$("#home_btn").removeClass("btn").addClass("selected");
	$("#about_btn,#cast_btn,#crew_btn").addClass("btn");
    $("#about,#cast,#crew").slideUp("Slow");
	$("#home").slideDown("Slow");
  });
  $("#about_btn").click(function(){
	$("#about_btn").removeClass("btn").addClass("selected");
	$("#home_btn").addClass("btn").removeClass("selected");
	$("#cast_btn,#crew_btn").addClass("btn");
    $("#home,#cast,#crew").slideUp("Slow");
	$("#about").slideDown("Slow");
  });
  $("#cast_btn").click(function(){
	$("#cast_btn").removeClass("btn").addClass("selected");
	$("#about_btn,#crew_btn,#home_btn").addClass("btn").removeClass("selected");
    $("#home,#about,#crew").slideUp("Slow");
	$("#cast").slideDown("Slow");
  });
  $("#crew_btn").click(function(){
	$("#crew_btn").removeClass("btn").addClass("selected");
	$("#cast_btn,#about_btn,#home_btn").addClass("btn").removeClass("selected");
    $("#home,#about,#cast").slideUp("Slow");
	$("#crew").slideDown("Slow");
  });
}); 

</script>
</head>
<body>
  <div id="menu">
  	<img src="style/img/explore.png" width="115" height="113"/>
    <a href="#home"><button id="home_btn">&nbsp;</button></a>
    <a href="#about"> <button id="about_btn">&nbsp;</button></a>
    <a href="#cast"><button id="cast_btn">&nbsp;</button></a>
    <a href="#crew"><button id="crew_btn">&nbsp;</button></a>
  </div>
  <div id="home">
  </div>
  <div id="about">
    <div class="wrapper">
    <?php echo $about;//include 'content/about.php';?>
    </div>
  </div>
  <div id="cast">
  <div class="wrapper">
    <?php echo $cast_list;//include 'content/cast.php';?>
    </div>
  </div>
  <div id="crew">
  <div class="wrapper">
    <?php echo $crew;//include 'content/crew.php';?>
    </div>
  </div>
  </div>
  <div id="footer">
    <div id="foot-content">
      <p>All rights reserved | NAPZA-movie.com | &copy;2013</p>
      <p><em>Design & development by</em> <a href="http://www.trihartanto.com">Threetopia</a> feat RedSmokegraphics</p>
    </div>
  </div>
</body>
</html>