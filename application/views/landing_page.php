<?php
$html  = '<!DOCTYPE html>';
$html .= '<html lang="en">';
$html .= '<head>';
$html .= '	<meta charset="utf-8">';
$html .= '	<meta content="NAPZA the movie official website" name="description">';
$html .= '	<meta content="napza,narkotika,movie,film" name="keywords">';
$html .= '	<style>';
$html .= '	
			@font-face
			{
				font-family: \'cust_font\';
				src: url(\''.base_url("style/font/btypewriter-webfont.eot").'\');
				src: url(\''.base_url("style/font/btypewriter-webfont.eot").'\') format(\'embedded-opentype\'),
					 url(\''.base_url("style/font/btypewriter-webfont.woff").'\') format(\'woff\'),
					 url(\''.base_url("style/font/btypewriter-webfont.ttf").'\') format(\'truetype\'),
					 url(\''.base_url("style/font/btypewriter-webfont.svg").'\') format(\'svg\');
				font-weight: normal;
				font-style: normal;
			}
			body
			{
				background:black;
				color:white;
			} 
			#wrap
			{
				display:block;
				margin:0 auto;
				width:800px;
			} 
			.box1
			{
				display:inline-block;
				font-family:cust_font;
				margin-top:200px;
				padding-left:130px;
				font-size:20px;
				float:left;
			} 
			.box2
			{
				display:inline-block;
				margin:0px;
				padding:0px;
				font-family:cust_font;
				font-size:100px;
				float:left;
				width:800px;
				text-align:center;
				color:red;
			}
			.box3
			{
				display:inline-block;
				margin:0px;
				padding:0px;
				font-family:cust_font;
				font-size:20px;
				float:left;
				width:670px;
				text-align:right;
			}
			.box4
			{
				display:inline-block;
				font-family:cust_font;
				margin:0px 0px 0px 0px;
				padding:0px 0px 0px 0px;
				font-size:20px;
				float:left;
				width:800px;
				text-align:center;
			}
			.cover
			{
				background:url(\''.base_url("style/images/cover.jpg").'\');
				height:500px;
				width:800px;
			}';
$html .= '	</style>';
$html .= '	<title>NAPZA the movie</title>';
$html .= '</head>';
$html .= '<body>';
$html .= '	<div id="wrap">';
//$html .= '		<div class="box1">';
//$html .= '			Nantikan segera...';
//$html .= '		</div>';
//$html .= '		<div class="box2">';
//$html .= '			NAPZA';
//$html .= '		</div>';
//$html .= '		<div class="box3">';
//$html .= '			the movie';
//$html .= '		</div>';
//$html .= '		<div class="box4">';
//$html .= '			Diangkat dari kisah nyata kelamnya kehidupan.';
//$html .= '		</div>';
$html .= '		<div class="cover">';
$html .= '		</div>';
$html .= '	</div>';
$html .= '</body>';
$html .= '</html>';
echo $html;
?>