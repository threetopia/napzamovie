<?php
class Content extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	private $cast_list = array(
								'Marsha Timothy',
								'Nadine Candrawinata',
								'Prisia Nasution',
								'Reza Rahardian',
								'Dion Idol',
								'Adipati Dolken',
								'T Reza',
								'Bucek Deep',#fix
								'Dewi Irawan',#fix
								'Tya Arifin',#fix
								'Hengky Tornado',#fix
								'Ray Sahetapy',#fix
								'Poppy Sovia',#fix
								'Vino G Sebastian',
								'Keke Soeryo',
								'Minati Atmanegara',
								'Widya Wati',
								'Paramita Rusady',
								'Agus Kuncoro',
								'Jefri Tambayong',
								'Marcello Djorgi',
								'Pierre Gruno',
								'Kadek Devi',
								'Poppy Sovia'
								);
	
	public function cast_process()
	{
		return $this->load->view('cast/list.php',array('cast_list'=>$this->cast_list),true);
	}
	
	public function about()
	{
		return $this->load->view('about.php',array('cast_list'=>$this->cast_list),true);
	}
	
	public function crew()
	{
		return $this->load->view('crew.php',NULL,true);
	}
	
	public function cast()
	{
		
	}
}
?>